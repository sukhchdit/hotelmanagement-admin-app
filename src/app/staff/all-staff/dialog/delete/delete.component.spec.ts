import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { DeleteStaffDialogComponent } from './delete.component';
describe('DeleteComponent', () => {
  let component: DeleteStaffDialogComponent;
  let fixture: ComponentFixture<DeleteStaffDialogComponent>;
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [DeleteStaffDialogComponent]
    }).compileComponents();
  }));
  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteStaffDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
