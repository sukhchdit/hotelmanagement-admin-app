import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormStaffDialogComponent } from './form-dialog.component';
describe('FormDialogComponent', () => {
  let component: FormStaffDialogComponent;
  let fixture: ComponentFixture<FormStaffDialogComponent>;
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [FormStaffDialogComponent]
    }).compileComponents();
  }));
  beforeEach(() => {
    fixture = TestBed.createComponent(FormStaffDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
