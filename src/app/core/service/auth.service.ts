import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../models/user';
import { environment } from 'src/environments/environment';
import { DBkeys } from 'src/app/services/shared/db-keys.service';
import { LocalStoreManager } from 'src/app/services/shared/local-store-manager.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;

  constructor(private http: HttpClient, private localStorageService: LocalStoreManager, private router: Router) {
    this.currentUserSubject = new BehaviorSubject<User>(
      JSON.parse(localStorage.getItem('currentUser'))
    );
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  login(username: string, password: string) {
    return this.http
      .post<any>(`${environment.apiUrl}/authenticate`, {
        username,
        password,
      })
      .pipe(
        map((user) => {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify(user));
          this.currentUserSubject.next(user);
          return user;
        })
      );
  }

  public setClaims(claims: any) {
    let authUser ={
      city:claims ? claims['city'] : null,
      country:claims ? claims['country'] : null,
      fullName:claims ? claims['name'] : null,
      email:claims ? claims['emails'][0] : null,
      firstName:claims ? claims['given_name'] : null,
      lastName:claims ? claims['family_name'] : null,
      jobTitle:claims ? claims['jobTitle'] : null,
      postalCode:claims ? claims['postalCode'] : null,
      state:claims ? claims['state'] : null,
      streetAddress:claims ? claims['streetAddress'] : null
    };
    this.localStorageService.savePermanentData(authUser, DBkeys.CURRENT_USER);
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
    return of({ success: false });
  }
}
