import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AllroomComponent } from './all-rooms/all-rooms.component';
import { AddRoomComponent } from './add-room/add-room.component';
import { EditRoomComponent } from './edit-room/edit-room.component';
import { MsalGuard } from '@azure/msal-angular';
import { CanActivateGuard } from '../services/shared/can-activate-guard.service';


const routes: Routes = [
  {
    path: 'all-rooms',
    component: AllroomComponent,
    canActivate: [CanActivateGuard, MsalGuard]
  },
  {
    path: 'add-room',
    component: AddRoomComponent,
    canActivate: [CanActivateGuard, MsalGuard]
  },
  {
    path: 'edit-room',
    component: EditRoomComponent,
    canActivate: [CanActivateGuard, MsalGuard]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RoomRoutingModule { }
