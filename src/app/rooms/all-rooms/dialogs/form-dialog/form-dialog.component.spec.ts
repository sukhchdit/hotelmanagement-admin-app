import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { FormRoomDialogComponent } from './form-dialog.component';

describe('FormRoomDialogComponent', () => {
  let component: FormRoomDialogComponent;
  let fixture: ComponentFixture<FormRoomDialogComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [FormRoomDialogComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormRoomDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
