import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DeleteRoomDialogComponent } from './delete.component';

describe('DeleteRoomDialogComponent', () => {
  let component: DeleteRoomDialogComponent;
  let fixture: ComponentFixture<DeleteRoomDialogComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [DeleteRoomDialogComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteRoomDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
