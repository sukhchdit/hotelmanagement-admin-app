import { NgModule } from '@angular/core';
import { Page404Component } from './authentication/page404/page404.component';
import { AuthLayoutComponent } from './layout/app-layout/auth-layout/auth-layout.component';
import { MainLayoutComponent } from './layout/app-layout/main-layout/main-layout.component';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './core/guard/auth.guard';
import { CanActivateGuard } from './services/shared/can-activate-guard.service';
import { MsalGuard, MsalRedirectComponent } from '@azure/msal-angular';
const routes: Routes = [
  {
    path: '',
     component: MainLayoutComponent,
    children: [
      // { path: '', redirectTo: '/authentication/signin', pathMatch: 'full' },
      {
        path: '',
        // canActivate: [CanActivateGuard, MsalGuard],
        loadChildren: () =>
          import('./dashboard/dashboard.module').then((m) => m.DashboardModule),
      },
      {
        path: 'dashboard',
        // canActivate: [CanActivateGuard, MsalGuard],
        loadChildren: () =>
          import('./dashboard/dashboard.module').then((m) => m.DashboardModule),
      },
      {
        path: 'calendar',
        canActivate: [CanActivateGuard, MsalGuard],
        loadChildren: () =>
          import('./calendar/calendar.module').then((m) => m.CalendarsModule),
      },
      {
        path: 'task',
        canActivate: [CanActivateGuard, MsalGuard],
        loadChildren: () =>
          import('./task/task.module').then((m) => m.TaskModule),
      },
      {
        path: 'contacts',
        canActivate: [CanActivateGuard, MsalGuard],
        loadChildren: () =>
          import('./contacts/contacts.module').then((m) => m.ContactsModule),
      },
      {
        path: 'email',
        canActivate: [CanActivateGuard, MsalGuard],
        loadChildren: () =>
          import('./email/email.module').then((m) => m.EmailModule),
      },
      {
        path: 'booking',
        
        loadChildren: () =>
          import('./booking/booking.module').then((m) => m.BookingModule),
      },
      {
        path: 'rooms',
        
        loadChildren: () =>
          import('./rooms/rooms.module').then((m) => m.RoomModule),
      },
      {
        path: 'departments',
        
        loadChildren: () =>
          import('./departments/departments.module').then(
            (m) => m.DepartmentsModule
          ),
      },
      {
        path: 'staff',
        
        loadChildren: () =>
          import('./staff/staff.module').then((m) => m.StaffModule),
      },
      // {
      //   path: 'apps',
      //   loadChildren: () =>
      //     import('./apps/apps.module').then((m) => m.AppsModule),
      // },
      // {
      //   path: 'widget',
      //   loadChildren: () =>
      //     import('./widget/widget.module').then((m) => m.WidgetModule),
      // },
      // {
      //   path: 'ui',
      //   loadChildren: () => import('./ui/ui.module').then((m) => m.UiModule),
      // },
      // {
      //   path: 'forms',
      //   loadChildren: () =>
      //     import('./forms/forms.module').then((m) => m.FormModule),
      // },
      // {
      //   path: 'tables',
      //   loadChildren: () =>
      //     import('./tables/tables.module').then((m) => m.TablesModule),
      // },
      // {
      //   path: 'media',
      //   loadChildren: () =>
      //     import('./media/media.module').then((m) => m.MediaModule),
      // },
      // {
      //   path: 'timeline',
      //   loadChildren: () =>
      //     import('./timeline/timeline.module').then((m) => m.TimelineModule),
      // },
      // {
      //   path: 'icons',
      //   loadChildren: () =>
      //     import('./icons/icons.module').then((m) => m.IconsModule),
      // },
      // {
      //   path: 'extra-pages',
      //   loadChildren: () =>
      //     import('./extra-pages/extra-pages.module').then(
      //       (m) => m.ExtraPagesModule
      //     ),
      // },
      // {
      //   path: 'maps',
      //   loadChildren: () =>
      //     import('./maps/maps.module').then((m) => m.MapsModule),
      // },
      // {
      //   path: 'multilevel',
      //   loadChildren: () =>
      //     import('./multilevel/multilevel.module').then(
      //       (m) => m.MultilevelModule
      //     ),
      // },
    ],
  },
  {
    path: 'authentication',
    component: AuthLayoutComponent,
    loadChildren: () =>
      import('./authentication/authentication.module').then(
        (m) => m.AuthenticationModule
      ),
  },
  { path: 'auth', component: MsalRedirectComponent },
  { path: '**', component: Page404Component },
];
@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
