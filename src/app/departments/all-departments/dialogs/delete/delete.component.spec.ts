import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DeleteDepartmentDialogComponent } from './delete.component';

describe('DeleteComponent', () => {
  let component: DeleteDepartmentDialogComponent;
  let fixture: ComponentFixture<DeleteDepartmentDialogComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [DeleteDepartmentDialogComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteDepartmentDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
