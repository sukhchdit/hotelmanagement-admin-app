import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { FormDepartmentDialogComponent } from './form-dialog.component';

describe('FormDialogComponent', () => {
  let component: FormDepartmentDialogComponent;
  let fixture: ComponentFixture<FormDepartmentDialogComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [FormDepartmentDialogComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormDepartmentDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
