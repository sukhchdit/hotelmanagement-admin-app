import { EventEmitter, Injectable, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { LocalStoreManager } from './local-store-manager.service';
import { DBkeys } from './db-keys.service';
import { JwtHelperService } from './jwt-helper.service';
import { CurrentUserViewModel } from '../../models/dtomodels/currentuserviewmodel.model';
import { AccessToken, LoginResponse } from '../../models/common/login-response.model';
import { PermissionValues } from '../../models/common/permission.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  @Output() siteListChanged: EventEmitter<any[]> = new EventEmitter();
  @Output() investigatorListChanged: EventEmitter<any[]> = new EventEmitter();
  @Output() accounTypeChangedChanged: EventEmitter<any> = new EventEmitter();

  private previousIsLoggedInCheck = false;
  private _loginStatus = new Subject<boolean>();
  public redirectUrl:string;

  constructor(private localStorage: LocalStoreManager, private router: Router) {
    this.initializeLoginStatus();
  }

  private initializeLoginStatus() {
    this.localStorage.getInitEvent().subscribe(() => {
      this.reevaluateLoginStatus();
    });
  }

  private reevaluateLoginStatus(currentUser?: CurrentUserViewModel) {
    let user = currentUser || this.localStorage.getDataObject<CurrentUserViewModel>(DBkeys.CURRENT_USER);
    let isLoggedIn = user != null;

    if (this.previousIsLoggedInCheck != isLoggedIn) {
      setTimeout(() => {
        this._loginStatus.next(isLoggedIn);
      });
    }

    this.previousIsLoggedInCheck = isLoggedIn;
  }

  public processLoginResponse(response: LoginResponse, rememberMe: boolean) {
    let accessToken = response.access_token;

    if (accessToken == null) {
      throw new Error("Received accessToken was empty");
    }

    let refreshToken = response.refresh_token || this.refreshToken;
    let expiresIn = response.expires_in;

    let tokenExpiryDate = new Date();
    tokenExpiryDate.setSeconds(tokenExpiryDate.getSeconds() + expiresIn);

    let accessTokenExpiry = tokenExpiryDate;

    let jwtHelper = new JwtHelperService();

    let decodedAccessToken = <AccessToken>jwtHelper.decodeToken(response.access_token);
    let permissions: PermissionValues[] = Array.isArray(decodedAccessToken.permission) ? decodedAccessToken.permission : [decodedAccessToken.permission];

    let user = new CurrentUserViewModel();
    user.email = decodedAccessToken.email;
    user.phone = decodedAccessToken.phone;
    user.name = decodedAccessToken.given_name;
    user.id = decodedAccessToken.unique_name;
    user.firstName = decodedAccessToken.firstName;
    user.lastName = decodedAccessToken.lastName;
    user.organizationContactId = decodedAccessToken.organizationContactId;
    user.userRole = decodedAccessToken.role;
    user.lastLoginDate = decodedAccessToken.lastLoginDate;
    //console.log("Test: " + decodedAccessToken.lastLoginDate);

    this.saveUserDetails(user, permissions, accessToken, refreshToken, accessTokenExpiry, rememberMe);

    this.reevaluateLoginStatus(user);
  }

  private saveUserDetails(user: CurrentUserViewModel, permissions: PermissionValues[], accessToken: string, refreshToken: string, expiresIn: Date, rememberMe: boolean) {
    if (rememberMe) {
      this.localStorage.savePermanentData(accessToken, DBkeys.ACCESS_TOKEN);
      this.localStorage.savePermanentData(refreshToken, DBkeys.REFRESH_TOKEN);
      this.localStorage.savePermanentData(expiresIn, DBkeys.TOKEN_EXPIRES_IN);
      this.localStorage.savePermanentData(permissions, DBkeys.USER_PERMISSIONS);
      this.localStorage.savePermanentData(user, DBkeys.CURRENT_USER);
    }
    else {
      this.localStorage.saveSyncedSessionData(accessToken, DBkeys.ACCESS_TOKEN);
      this.localStorage.saveSyncedSessionData(refreshToken, DBkeys.REFRESH_TOKEN);
      this.localStorage.saveSyncedSessionData(expiresIn, DBkeys.TOKEN_EXPIRES_IN);
      this.localStorage.saveSyncedSessionData(permissions, DBkeys.USER_PERMISSIONS);
      this.localStorage.saveSyncedSessionData(user, DBkeys.CURRENT_USER);
    }

    this.localStorage.savePermanentData(rememberMe, DBkeys.REMEMBER_ME);
  }

  updateCurrentUserRole(currentUser) {
    var rememberMe = this.localStorage.getData(DBkeys.REMEMBER_ME);
    if (rememberMe)
      this.localStorage.savePermanentData(currentUser, DBkeys.CURRENT_USER);
    else
      this.localStorage.saveSyncedSessionData(currentUser, DBkeys.CURRENT_USER);
  }

  logout(): void {
    this.localStorage.deleteData(DBkeys.ACCESS_TOKEN);
    this.localStorage.deleteData(DBkeys.REFRESH_TOKEN);
    this.localStorage.deleteData(DBkeys.TOKEN_EXPIRES_IN);
    this.localStorage.deleteData(DBkeys.USER_PERMISSIONS);
    this.localStorage.deleteData(DBkeys.CURRENT_USER);
    this.localStorage.deleteData(DBkeys.User_Session);
    this.localStorage.deleteData(DBkeys.INFOLIST);

    this.reevaluateLoginStatus();
    this.router.navigate(['/']);
  }

  get accessToken(): string {
    this.reevaluateLoginStatus();
    return this.localStorage.getData(DBkeys.ACCESS_TOKEN);
  }

  get refreshToken(): string {
    this.reevaluateLoginStatus();
    return this.localStorage.getData(DBkeys.REFRESH_TOKEN);
  }

  get currentUser(): CurrentUserViewModel {
    let user = this.localStorage.getDataObject<CurrentUserViewModel>(DBkeys.CURRENT_USER);
    this.reevaluateLoginStatus(user);

    return user;
  }

  get isLoggedIn(): boolean {
    return this.currentUser != null;
  }

  updateUserInfo(user: CurrentUserViewModel) {
    if (Boolean(this.localStorage.getData(DBkeys.REMEMBER_ME)))
      this.localStorage.savePermanentData(user, DBkeys.CURRENT_USER);
    else
      this.localStorage.saveSyncedSessionData(user, DBkeys.CURRENT_USER);
  }

  public setClaims(claims: any) {
    let authUser ={
      city:claims ? claims['city'] : null,
      country:claims ? claims['country'] : null,
      fullName:claims ? claims['name'] : null,
      email:claims ? claims['emails'][0] : null,
      firstName:claims ? claims['given_name'] : null,
      lastName:claims ? claims['family_name'] : null,
      jobTitle:claims ? claims['jobTitle'] : null,
      postalCode:claims ? claims['postalCode'] : null,
      state:claims ? claims['state'] : null,
      streetAddress:claims ? claims['streetAddress'] : null
    };
    this.localStorage.savePermanentData(authUser, DBkeys.CURRENT_USER);
  }

}
