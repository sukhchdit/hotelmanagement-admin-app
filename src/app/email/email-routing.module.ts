import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InboxComponent } from './inbox/inbox.component';
import { ComposeComponent } from './compose/compose.component';
import { ReadMailComponent } from './read-mail/read-mail.component';
import { MsalGuard } from '@azure/msal-angular';
import { CanActivateGuard } from '../services/shared/can-activate-guard.service';
const routes: Routes = [
  {
    path: 'inbox',
    component: InboxComponent,
    canActivate: [CanActivateGuard, MsalGuard]
  },
  {
    path: 'compose',
    component: ComposeComponent,
    canActivate: [CanActivateGuard, MsalGuard]
  },
  {
    path: 'read-mail',
    component: ReadMailComponent,
    canActivate: [CanActivateGuard, MsalGuard]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmailRoutingModule {}
